# Drupal 8 Outsourcer Test

This is meant to provide starter code for the outsourcer test. This project is based on composer. If you are handling both the backend and frontend portion, take a look at both of the sections below.

*  [General Specifications](#markdown-header-general-specifications)
*  [Frontend Specifications](#markdown-header-frontend-specifications)
*  [Backend Specifications](#markdown-header-backend-specifications)

## General Specifications

*  Grab the code from the master branch of this repo. Review the main readme in this branch that outlines the specs for the project.
*  Depending on which parts we are having you do, take a look at the Frontend Specifications and Backend Specifications sections below.
*  Apply the necessary changes locally. Make sure to export any Drupal 8 configuration via configuration files.
*  Email us the following:
    *  All of the files, including what is in the repo and user files. We should be able to fully test your code locally.
    *  A database dump of the db you are using.
    *  Provide necessary urls and info in how we can test each piece of your code.
*  We will review the code and site locally and let you know if we have any questions.
*  For both the frontend and backend pieces, please try to follow drupal best practices and coding standards...as much as is reasonable. :)

## Frontend Specifications

1.  Create a new Drupal 8 theme. The colors of the theme should generally match the comps below. Don't worry about adding a logo.
2.  Create some testing basic pages with testing content, and link to them in the main menu that should output in the theme.
3.  All CSS should be in Sass. Provide instructions in how we can process the Sass.
4.  Create a new content type called `Custom Article` with the following fields:
    *  Title
    *  Body with Summary (WYSIWYG)
    *  Image (media field)
5.  Populate the `Custom Article` content type with testing content from the `Devel Generate` module.
6.  The home page should do the following (with a view):
    *  List the 10 most recent `Custom Article` nodes using the teaser view mode. The teaser should display a small image, title and summary from the content.
    *  Should paginate the results.
    *  The home page should look like this: ???
7.  Style the full page view mode for `Custom Article` nodes to make them look like this: ???
    *  Clicking the image on the full page view mode should display the full image in a modal. You can have this modal styled however you want.
    *  Let us know which modal library or drupal module you are using.
8.  Make the site responsive, so that things look good in mobile, tablet and desktop.

## Backend Specifications

The backend portion of this is similar to what is happening on the home page for the frontend specs, but this is meant to be done 100% within a custom module. If you are only doing the backend part of this test, don't worry about creating a custom theme (you can just use Bartik) and you don't have to worry about styling this page or handling the full page display for the node links on this page.

1.  Create a new custom Drupal 8 module to store your code.
2.  If you haven't already, create the content type listed above in the Frontend Specifications, items #4 and #5.
3.  Create a page in this new custom module that does the following. Do not use Views for this.
    *  List the 10 most recent `Custom Article` nodes.
    *  Should paginate the results.
    *  Output the results using the teaser view mode.
    *  Provide a drop down filter on the frontend that allows them to change how many results are shown on each page (All, 10, 50). Default to 10. This should be done with AJAX and there shouldn't be a submit button (trigger ajax via on change).
4.  Let us know the url where we can access the new page.
