# Docksal Hosting Integration

This builds on top of the awesome [Docksal](https://docksal.io/) project by providing custom commands that have more integration with certain hosting providers and content management systems. See the [Project Goals](#markdown-header-project-goals) for what we are trying to accomplish.

*  [Project Goals](#markdown-header-project-goals)
*  [Project Setup](#markdown-header-project-setup)
    *  [Installing Docksal](#markdown-header-installing-docksal)
    *  [Restarting Computer](#markdown-header-restarting-computer)
    *  [Startup for a Docksal Project](#markdown-header-startup-for-a-docksal-project)
*  [Initial Project Setup](#markdown-header-initial-project-setup)
*  [Drush + WP CLI](#markdown-header-drush-wp-cli)
*  [Mysql](#markdown-header-mysql)
*  [Email](#markdown-header-email)
*  [Solr](#markdown-header-solr)
*  [Docksal Basic Commands](#markdown-header-docksal-basic-commands)
*  [Custom Commands](#markdown-header-custom-commands)
*  [Updating Docksal Hosting Integration](#markdown-header-updating-docksal-hosting-integration)
*  [Host Type: Other](#markdown-header-host-type-other)

For a brief run down in which commands to run under which use cases, view this 
[google doc](https://docs.google.com/document/d/1srWbU7hkHj9yZ9SzIP9bgvwgzXhaunZNfoXmMWtIXR0).

## Project Goals
Docksal makes it simple to get a local site up and running. We want to extend that functionality by providing custom commands that do the following:

- Have the local environment closely match things like software versions and settings.
- Once this is setup on a project, we want it to be very easy and quick for other developers to get the site fully working locally.
- Make it simple and fast to pull down the latest db + user files from certain hosting providers.
- Have the full frontend process in compiling Sass, processing JS, etc... 100% available on the CLI container. Once this is setup to work on a project, it will work the same for all devolopers without them having to worry about installing these tools on their host computer.
- Provide custom commands that streamline things like resetting config, clearing caches, watching/processing sass+js, etc... to speed up development.
- Make it easy to provide customizations per project, with keeping the ability to update the core code easily via composer.

> This is meant to be a great option if you have a project that is in Drupal 7, Drupal 8 or Wordress, and is hosted on Platform.sh or Pantheon. If you just need a quick local dev site setup to play with, I suggest using the `fin project create` command from Docksal.

**Here is how some of the commands look with a Drupal 8 site connected to Platform.sh:**

![Example #1](resources/example1.png)

![Example #2](resources/example2.png)

**Supported Hosting Providers**

- Platform.sh
- Pantheon
- Other: Doesn't provide pulling down db + user files out of the box, but does make it simple to get a local working for non supported hosts.

**Supported Content Management Systems**

- Drupal 8
- Drupal 7
- Wordpress

## Project Setup
If Docksal has already been setup on a project and is in the repo, follow these steps to easily get up and running.

### Installing Docksal
You only need to do this once after first installing Docksal.

Go to the [Installing Docksal](https://docs.docksal.io/en/develop/getting-started/setup/) docs, and follow the instructions under `macOS with VirtualBox`. Once you have Docksal installed, you can run `fin update` to download the latest version.

> If you have more than 8GB ram on your machine, it is recommended that you [increase the Docksal ram size](https://docs.docksal.io/en/master/advanced/vm/#set-docksals-virtualbox-vm-memory-ram) by running `fin vm ram 4096`. Not totally necessary, but it might make things run a little faster.

If you have projects on Platform.sh or Pantheon, you will want to create and set the global token for each service. See the [platform api token docs](https://docs.platform.sh/gettingstarted/cli/api-tokens.html) and the [pantheon machine token docs](https://pantheon.io/docs/machine-tokens/) in how to generate these tokens. Then use those token values in the commands below. These global variables only need to be setup once and not per project.

**Platform.sh Token**: `fin config set --global SECRET_PLATFORMSH_CLI_TOKEN="[TOKEN]"`

**Pantheon Token**: `fin config set --global SECRET_TERMINUS_TOKEN="[TOKEN]"`

### Restarting Computer

After installing Docksal, when restarting your computer (or just starting out), make sure to run `fin vm start`.

### Startup for a Docksal Project

> If this is your first time starting up docksal on this project, but you already had the project checked out, it is recommended that you first delete your repo files and re-checkout the code. That way it will delete the local setting file that might already be in there and copy the default settings file from this repo. At that point you can re-copy any config you want to include in the settings file copied from the .docksal directory.

1.  If you haven't [installed Docksal](#markdown-header-installing-docksal), do that now. Otherwise skip.
2.  If you haven't run `fin vm start` since a restart, run that now. Otherwise skip.
3.  Checkout the code for the project then `cd` into the project directory.
4.  Run: `fin p start`. Make a note of the url it displays at the end.
5.  Run: `fin data`. This will run through all of the setup steps.
6.  The local site should load!

View the [Drush + WP CLI](#markdown-header-drush-wp-cli), [Mysql](#markdown-header-mysql), [Docksal Basic Commands](#markdown-header-docksal-basic-commands) and [Custom Commands](#markdown-header-custom-commands) sections for more details in how to interact with the project.

> Once you have gone through the above steps, the only thing you need to do when comming back to the project is run `fin p start`. If you wipe out the project with `fin p remove`, you will need to run `fin data` again (or if you want to grab the latest db).

> To stop a project from running, run `fin p stop`. You will not lose the database or files. You can have several projects running at the same time.

> When switching branches on a project, or when pulling down changes, you will want to run `fin local`. This will make sure your db config matches code and clears drupal caches.

## Initial Project Setup
If you need to setup Docksal on a project (whether it is a new build or an existing site), follow the below steps. You only need to do this once per project. This assumes you are using composer.

> If you are integrating this into an existing project repo, you may want to delete the repo folder from your computer first and then re-clone...just to make sure you wipe out any local setting files you may have. If you have any of these local settings files committed to the repo, you will want to delete them first.

> If there isn't a `composer.json` file already, you will first want to create a basic one in your project root with `composer init`. Don't add any dependencies from those setup steps.

1. Add the following "scripts" to your `composer.json` file. This ensures it copies the docksal files to the `.docksal` folder and the files will get updated properly when we add it via composer.

        {
            "scripts": {
                "post-update-cmd": ["vendor/bin/docksal-hosting-integration-install-update"],
                "post-install-cmd": ["vendor/bin/docksal-hosting-integration-install-update"]
            }
        }

2. Install the project via composer: `composer require --dev thebrickfactory/docksal-hosting-integration`. You should end up seeing `Updated the .docksal files!` and you should have a `.docksal` directory in your project root.
3. Update the variables in `.docksal/docksal-local.env`. Read through all the options! These changes will not get overridden with updates. To update php.ini settings, the default drupal/wp setting files or have code run during some of the custom commands for this project, see the [overrides readme docs](overrides/README.md).
4. Commit the `.docksal`, `.gitignore` and composer files updated for this project.
5. Run through the [Startup for a Docksal Project](#markdown-header-startup-for-a-docksal-project) steps above.

## Drush + WP CLI
You can run drush commands with `fin drush`. Example: `fin drush cr` or `fin drush cc all`. If fin drush can't connect to the drupal site from the project root, go into the document root folder and try from there.

For WP sites you can run WP CLI commands with `fin wp`. Example: `fin wp cache flush`

## Mysql
Adminer is installed. After you run through the project setup, you can access this through `http://adminer.[site].docksal`. You can run `fin vhosts` to see a list of all available urls for a project, including the adminer url.

Below are the mysql creds:

**Database User**: user  
**Database Password**: user  
**Database Name**: default

If you are on a mac and have sequelpro installed, you can run `fin sequelpro` and it will open that program and connect to the project database.

## Email
By default, emails are sent through [Mail Hog](https://docs.docksal.io/en/master/tools/mailhog/#enabling-mailhog-service). All emails will be delivered and accessible through `http://mail.[site].docksal`. You can "release" emails from there if you want the emails to be sent to the specified email address for testing (but you will have to connect to an SMTP server). This is useful for testing sent emails locally.

## Solr
To get your your local to work with solr, you will need to follow the below steps. Eventually we want to include default code in the setting files to make this easier to do, but we haven't needed to get solr working on a Docksal site yet. So this is a general workflow in what you need to do.

1.  Get the necessory solr drupal modules installed and configured in the host dev environment. Make sure to export the config used there so that is in code.
2.  Open up the `.docksal/docksal-local.yml` file and uncomment the code below `# Uncomment the code below to enable solr.`. Restart the project and the solr service should be started.
3.  Update your settings.local.php file to [update the solr config](https://docksal.readthedocs.io/en/master/tools/apache-solr/) to work with solr.
4.  Index the content via the admin gui for solr.

## Docksal Basic Commands
Below is a list of the main Docksal commands you will want to be aware of. I grouped the related commands in each row. You can always run `fin help` to see a list of all commands + custom commands.

1.  `fin vm start` / `fin vm stop` / `fin vm restart`: If you are not using the native docker option, this will start/setup the vm in virtual box. You will want to start this up after a computer restart, but it only needs to be started once per project.
2.  `fin p start` / `fin p stop` / `fin p restart`: You run these commands inside of the Docksal project repo directory. This will boot up the docker images. The stop/restart command will not wipe out the data in the docker containers. One thing I noticed is that if you have a Docksal project setup, and if it is not running...when you go to the Docksal url for that project in the browser, it will start up the project automatically.
3.  `fin p remove`: This deletes the docker images for this project, and you will lose the database and data on the docker images. You will only want to run this if you are having local site issues or want to start the project from scratch.
4.  `fin bash`: This accesses the cli docker image for the project. From here you can run platform, drush, wp, node, gulp, yarn, composer, etc... directly on the docker image.
5.  `fin sequelpro`: If you are on a mac and have sequelpro installed, this will launch the program and connect you directly to the project database.
6.  `fin p create`: Uses the Docksal boilerplate repos to quickly setup a local site. You can use these to setup drupal 8, drupal 7, etc... easily. But we don't use this as part of a new project setup.
7.  `fin update`: Updates Docksal and the base docker images.
8.  `fin pl`: List all Docksal projects that are running (that have been started).
9.  `fin stop --all`: Stops all running Docksal projects. This will not wipe out any data on these projects.

## Custom Commands
We created custom commands to make your live easier.

*  `fin data`: This is meant as the main command to run when you are first setting up the project or when you want to get the latest db + user files. **It will wipe out the database + user files if they already exist!** It downloads the db + user files, runs `fin init` and `fin local` (with no arguments it uses the default environment set in the docksal-local.env file). There are a few options you can send:
    *  `fin data db`: Runs the build + imports the latest db only.
    *  `fin data files`: Runs the build + imports the user files only.
    *  `fin data [environment]`: Runs the build and mports the db + user files from a specific environment. Example: `fin data master`
*  `fin init`: Runs the build, which includes composer install, yarn install and installs dependencies on the server. `fin data` will end up running this command automatically.
*  `fin local`: This resets your db config to match what is in code and clears caches. You will want to run this when switching branches or pulling down new code from the repo. This automatically runs in `fin data`.
*  `fin import-db`: Imports the database from an environment. With no arguments, uses the default environment set in the docksal-local.env file. You can send a few options:
    *  `fin import-db [environment]`: Imports the db from a specific environment and uses the cached db if we have one. Example: `fin import-db master`
    *  `fin import-db [environment] true`: Imports the db from a specific environment and will download the latest db. Example: `fin import-db master true`
*  `fin import-user-files`: Downloads the user files. You can send an environment option.
    * `fin import-user-files [environment]`: Downloads the user files from a specific environment. Example: `fin import-user-files master`
*  **Utility Commands**: 
   There are some other commands that are designed to be used with certain kinds of sites or hosting providers.
    *  `fin drush [command]`: Run drush commands against the Drupal site.
    *  `fin wp [command]`: Run WP CLI commands against the Wordpress site.
    *  `fin watch`: Runs `yarn start` in the theme directory.
    *  `fin gulp [command]`: Runs gulp in the theme directory.
    *  `fin yarn [command]`: Runs yarn commands in the theme directory.
    *  `fin deploy [environment]`: Pushes the code to the hosting environment. This does a git force push. Useful if you have the main repo in bitbucket/github and the site is hosted in Platform.sh or Pantheon.

## Updating Docksal Hosting Integration
To update the `.docksal` files to the latest code, update the version in your composer.json file, and then run `composer update thebrickfactory/docksal-hosting-integration`. Your project configuration files and overrides should not be deleted. You can set a specific version to use in your `composer.json` file. You can also update Docksal by running `fin update`.

## Host Type: Other
This is a special host type in that it isn't designed to automatically pull down the db + user files. However, it is setup to import this data when it detects these files:

- `db/db.sql` or `db/db.sql.gz`
- `db/files.tar.gz`: The contents of the files directory should be directly in this compressed file.

Everything else about this host type works the same as the other host types, and it provides a fallback option if you are working on a site that isn't connected to a supported host. You could also create override commands to automatically get this data a different way.
