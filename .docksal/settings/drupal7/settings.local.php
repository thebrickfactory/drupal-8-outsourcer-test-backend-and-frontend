<?php
/**
 * @file
 * Default drupal settings file to work with docksal.
 */

// Add your database configuration here (and uncomment the block).
$databases['default']['default'] = array(
  'driver' => 'mysql',
  'host' => 'db',
  'username' => 'user',
  'password' => 'user',
  'database' => 'default',
  'prefix' => '',
);

$conf['cache'] = 0;                       // Page cache
$conf['page_cache_maximum_age'] =  0;     // External cache TTL
$conf['preprocess_css'] = FALSE;          // Optimize css
$conf['preprocess_js'] = FALSE;           // Optimize javascript
$conf['views_skip_cache'] = TRUE;         // Views caching

// Use the docksal VIRTUAL_HOST value.
$base_url = 'http://' . getenv('VIRTUAL_HOST');

// Require the site loads over https if config value set.
if ($_SERVER['INTEGRATION_FORCE_HTTPS'] === 'true') {
  $base_url = 'https://' . getenv('VIRTUAL_HOST');
  if (!isset($_SERVER['HTTPS']) && php_sapi_name() != "cli" && $_SERVER["HTTP_X_FORWARDED_PROTO"] != 'https') {
    header('HTTP/1.0 301 Moved Permanently');
    header('Location: https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    exit();
  }
}
