#!/bin/bash

## Drupal 7 init.sh script.

## Copies the default setting file to the correct location.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

progressMessage "Drupal 7: Copying setting files..."

## Define all of the path options.
PATH_FINAL_SETTINGS="${PROJECT_ROOT}/${DOCROOT}/sites/default/settings.local.php"
PATH_DEFAULT_SETTINGS="${PROJECT_ROOT}/.docksal/settings/drupal7/settings.local.php"
PATH_OVERRIDES_SETTINGS="${PROJECT_ROOT}/.docksal/overrides/settings/settings.local.php"

# I don't like having to include host type config in here, but we need to move
# the settings.php file to a different location in Platform.sh.
if [ "${INTEGRATION_HOST_TYPE}" == "platform" ] && [ ! -f "${PROJECT_ROOT}/.platform/local/shared/settings.local.php" ] ; then
  if [ ! -d "${PROJECT_ROOT}/.platform/local" ] ; then
    mkdir "${PROJECT_ROOT}/.platform/local"
  fi

  if [ ! -d "${PROJECT_ROOT}/.platform/local/shared/" ] ; then
    mkdir "${PROJECT_ROOT}/.platform/local/shared/"
  fi
  PATH_FINAL_SETTINGS="${PROJECT_ROOT}/.platform/local/shared/settings.local.php"
fi

## If the override files exist, use those instead of the default.
if [ -f "${PATH_OVERRIDES_SETTINGS}" ]; then
  echo "Found override settings file."
  PATH_DEFAULT_SETTINGS="${PATH_OVERRIDES_SETTINGS}"
fi

## Copy the setting file if it isn't already there.
if [ ! -f "${PATH_FINAL_SETTINGS}" ]; then
  sudo cp "${PATH_DEFAULT_SETTINGS}" "${PATH_FINAL_SETTINGS}"
  sudo chmod 0777 "${PATH_FINAL_SETTINGS}"
  echo "Copied '${PATH_DEFAULT_SETTINGS}' to '${PATH_FINAL_SETTINGS}'."
else
  echo "The file settings.local.php is already there."
fi
