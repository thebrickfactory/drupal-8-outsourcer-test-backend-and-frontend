#!/bin/bash

## Drupal 8 init.sh script.

## Copies the default setting files to the correct locations.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

progressMessage "Drupal 8: Copying setting files..."

## Define all of the path options.
PATH_FINAL_SETTINGS="${PROJECT_ROOT}/${DOCROOT}/sites/default/settings.local.php"
PATH_FINAL_SERVICES="${PROJECT_ROOT}/${DOCROOT}/sites/default/local.services.yml"
PATH_DEFAULT_SETTINGS="${PROJECT_ROOT}/.docksal/settings/drupal8/settings.local.php"
PATH_DEFAULT_SERVICES="${PROJECT_ROOT}/.docksal/settings/drupal8/local.services.yml"
PATH_OVERRIDES_SETTINGS="${PROJECT_ROOT}/.docksal/overrides/settings/settings.local.php"
PATH_OVERRIDES_SERVICES="${PROJECT_ROOT}/.docksal/overrides/settings/local.services.yml"

## If the override files exist, use those instead of the default.
if [ -f "${PATH_OVERRIDES_SETTINGS}" ]; then
  echo "Found override settings file."
  PATH_DEFAULT_SETTINGS="${PATH_OVERRIDES_SETTINGS}"
fi

if [ -f "${PATH_OVERRIDES_SERVICES}" ]; then
  echo "Found override services file."
  PATH_DEFAULT_SERVICES="${PATH_OVERRIDES_SERVICES}"
fi

## Copy the setting files if they aren't already there.
if [ ! -f "${PATH_FINAL_SETTINGS}" ]; then
  sudo cp "${PATH_DEFAULT_SETTINGS}" "${PATH_FINAL_SETTINGS}"
  sudo chmod 0777 "${PATH_FINAL_SETTINGS}"
  echo "Copied '${PATH_DEFAULT_SETTINGS}' to '${PATH_FINAL_SETTINGS}'."
else
  echo "The file settings.local.php is already there."
fi

if [ ! -f ${PATH_FINAL_SERVICES} ]; then
  sudo cp "${PATH_DEFAULT_SERVICES}" ${PATH_FINAL_SERVICES}
  sudo chmod 0777 ${PATH_FINAL_SERVICES}
  echo "Copied '${PATH_DEFAULT_SERVICES}' to '${PATH_FINAL_SERVICES}'."
else
  echo "The file local.services.yml is already there."
fi
