#!/bin/bash

## Drupal 8 import-db.sh script.

## Imports a db dump into the Drupal 8 site. We assume one of these types of sql 
## backup files exist:
## - ${PROJECT_ROOT}/db/${INTEGRATION_HOST_TYPE}.${ENVIRONMENT}.sql.gz
## - ${PROJECT_ROOT}/db/${INTEGRATION_HOST_TYPE}.${ENVIRONMENT}.sql

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"
DB_FILENAME="${PATH_DB_BASE_CLI}/${2}"

## Drop the current local database.
echo "Dropping old database..."
drush sql-drop -y

DATABASE_FILE1="${DB_FILENAME}.sql.gz"
DATABASE_FILE2="${DB_FILENAME}.sql"

## Import the database backup.
# Depending on the type of of the backup sql file, run different commands.
if [ -f "${DATABASE_FILE1}" ] ; then
  echo "Found exported .sql.gz file. Importing the data."
  gunzip -k < ${DATABASE_FILE1} | drush sql-cli
elif [ -f "${DATABASE_FILE2}" ] ; then
  echo "Found exported .sql file. Importing the data."
  drush sql-cli < ${DATABASE_FILE2}
else
  faileMessage "Could not import db. DB backup file doesn't exist! Stopping."
fi
