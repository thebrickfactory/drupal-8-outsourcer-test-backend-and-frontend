#!/bin/bash

## Wordpress import-db.sh script.

## Imports a db dump into the Wordpress site. We assume one of these types of
## sql backup files exist:
## - ${PROJECT_ROOT}/db/${INTEGRATION_HOST_TYPE}.${ENVIRONMENT}.sql.gz
## - ${PROJECT_ROOT}/db/${INTEGRATION_HOST_TYPE}.${ENVIRONMENT}.sql

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"
DB_FILENAME="${PATH_DB_BASE_CLI}/${2}"

# Go into document root to access WP CLI for the local.
cd "$PROJECT_ROOT/$DOCROOT"

## Drop the current local database.
echo "Dropping old database..."
wp db reset --yes

DATABASE_FILE1="${DB_FILENAME}.sql.gz"
DATABASE_FILE2="${DB_FILENAME}.sql"

## Import the database backup.
# Depending on the type of of the backup sql file, run different commands.
if [ -f "${DATABASE_FILE1}" ] ; then
  echo "Found exported .sql.gz file. Importing the data."
  gunzip -k < ${DATABASE_FILE1} | wp db import -
elif [ -f "${DATABASE_FILE2}" ] ; then
  echo "Found exported .sql file. Importing the data."
  wp db import ${DATABASE_FILE2}
else
  faileMessage "Could not import db. DB backup file doesn't exist! Stopping."
fi

## Update the site url so we don't get redirected to pantheon!
if $INTEGRATION_FORCE_HTTPS ; then
  SITE_URL="https://${VIRTUAL_HOST}"
else
  SITE_URL="http://${VIRTUAL_HOST}"
fi

wp option update home "${SITE_URL}"
wp option update siteurl "${SITE_URL}"
