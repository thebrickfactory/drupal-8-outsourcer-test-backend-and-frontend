#!/bin/bash

## Wordpress init.sh script.

## Define all of the path options.
PATH_FINAL_SETTINGS="${PROJECT_ROOT}/${DOCROOT}/wp-config-local.php"
PATH_DEFAULT_SETTINGS="${PROJECT_ROOT}/.docksal/settings/wp/wp-config-local.php"
PATH_OVERRIDES_SETTINGS="${PROJECT_ROOT}/.docksal/overrides/settings/wp-config-local.php"

## If the override files exist, use those instead of the default.
if [ -f "${PATH_OVERRIDES_SETTINGS}" ]; then
  echo "Found override settings file."
  PATH_DEFAULT_SETTINGS="${PATH_OVERRIDES_SETTINGS}"
fi

## Copy the setting file if it isn't already there.
if [ ! -f "${PATH_FINAL_SETTINGS}" ]; then
  sudo cp "${PATH_DEFAULT_SETTINGS}" "${PATH_FINAL_SETTINGS}"
  sudo chmod 0777 "${PATH_FINAL_SETTINGS}"
  echo "Copied wp-config-local.php."
else
  echo "Copied '${PATH_DEFAULT_SETTINGS}' to '${PATH_FINAL_SETTINGS}'."
fi
