#!/bin/bash

## Wordpress local.sh script.

## In Wordpress CLI, clears the cache.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

progressMessage "Run wp cache flush..." && wp cache flush
