#!/bin/bash

## Pantheon import-db.sh script.

## Downloads the latest database dump from a Pantheon environment.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"
DB_FILENAME="${PATH_DB_BASE_CLI}/${2}.sql.gz"

# Technically, Pantheon doesn't really support multiple dbs. So we don't use
# the INTEGRATION_DATABASE variable.

# Download the latest db backup from the pantheon environment, if cached file
# doesn't exist.
if [ ! -f ${DB_FILENAME} ] ; then
  terminus backup:get $INTEGRATION_SITE_ID.$ENVIRONMENT --element=db --to="${DB_FILENAME}" -y
  echo "Exported database."
fi
