#!/bin/bash

## Pantheon import-user-files.sh script.

## Downloads the latest user files backup from a Pantheon environment.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"
USER_FILES_PATH="$PROJECT_ROOT/${DOCROOT}/${2}"
COMPRESSED_FILE="files.tar.gz"

progressMessage "Downloading the user files from Pantheon..."

## Download the latest user files backup from the pantheon environment.
terminus backup:get $INTEGRATION_SITE_ID.$ENVIRONMENT --element=files --to="${PROJECT_ROOT}/db/${COMPRESSED_FILE}" -y

## Make sure the compressed file exists.
if [ ! -f "$PROJECT_ROOT/db/${COMPRESSED_FILE}" ] ; then
  echo "No compressed file for the user files found at 'db/${COMPRESSED_FILE}'. Not importing user files."
  exit
fi

# Delete the user files directory if it exists.
if [ -d "${USER_FILES_PATH}" ] ; then
  sudo rm -R -f "${USER_FILES_PATH}"
fi

# Extract and move to correct location.
echo "Extracting 'db/${COMPRESSED_FILE}' to '${USER_FILES_PATH}'..."
cd "${PROJECT_ROOT}/db"
tar -xzf "${COMPRESSED_FILE}"
mv "files_$ENVIRONMENT" "${USER_FILES_PATH}"
rm files.tar.gz

# Update file permsions.
sudo chmod -R 777 "${USER_FILES_PATH}"
