#!/bin/bash

## Pantheon init.sh script.

## The CLI will already authenticate with terminus from the global token, so we
## don't need to do anything there. And terminus doesn't really have a "build"
## command. So we are really kind of just making this as general as possible.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

cd "${PROJECT_ROOT}"

## Run the build.
if [ -e "${PROJECT_ROOT}/composer.json" ] ; then
  progressMessage "Running composer install..."
  composer install
else
  echo "No composer.json file found. Nothing to build."
fi
