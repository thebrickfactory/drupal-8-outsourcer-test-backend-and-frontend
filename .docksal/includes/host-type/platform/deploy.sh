#!/bin/bash

## Platform.sh deploy.sh script.

## Pushes code to a Platform.sh environment.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"

progressMessage "Starting push to the site id '${INTEGRATION_SITE_ID}' on the Platform.sh environment '${ENVIRONMENT}'. This could take a while..."
platform environment:push --project="${INTEGRATION_SITE_ID}" --environment="${ENVIRONMENT}" --target="${ENVIRONMENT}" --force --wait -y

progressMessage "Platform urls:"
platform environment:url --project="${INTEGRATION_SITE_ID}" --environment="${ENVIRONMENT}" --browser=0 --pipe -y
