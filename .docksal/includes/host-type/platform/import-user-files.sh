#!/bin/bash

## Platform import-user-files.sh script.

## Downloads the user files from a Platform.sh environment.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"
USER_FILES_PATH="${2}"

progressMessage "Downloading the user files from Platform.sh..."

## Download the user files from the platform environment.
platform mount:download --project="${INTEGRATION_SITE_ID}" --environment="${ENVIRONMENT}" --mount="${INTEGRATION_USER_FILES_MOUNT_PATH}/${USER_FILES_PATH}" --target="${PROJECT_ROOT}/${DOCROOT}/${USER_FILES_PATH}" -y

# Update file permsions.
sudo chmod -R 777 "${PROJECT_ROOT}/${DOCROOT}/${USER_FILES_PATH}"
