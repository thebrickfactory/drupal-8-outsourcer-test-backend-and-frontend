#!/bin/bash

## Platform init.sh script.

## Authenticates with the platform cli and runs "platform build".

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

## Run the build.
progressMessage "Running platform build..."

# Make sure the files + folders in .platform are writable.
if [ -d "${PROJECT_ROOT}/.platform" ] ; then
  sudo chmod -R 0777 "${PROJECT_ROOT}/.platform"
fi

platform build
