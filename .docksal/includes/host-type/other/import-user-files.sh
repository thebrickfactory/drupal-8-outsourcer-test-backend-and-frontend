#!/bin/bash

## Other import-user-files.sh script.

## We are not connected to a hosting provider to grab the user files
## automatically. Instead we look for the db/files.tar.gz file and extract that
## into the user files directory.

# Get the global shared file.
source "${PROJECT_ROOT}/.docksal/includes/global.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"
USER_FILES_PATH="$PROJECT_ROOT/${DOCROOT}/${2}"
COMPRESSED_FILE="files.tar.gz"

## Make sure the compressed file exists.
if [ ! -f "$PROJECT_ROOT/db/${COMPRESSED_FILE}" ] ; then
  echo "No compressed file for the user files found at 'db/${COMPRESSED_FILE}'. Not importing user files."
  exit
fi

progressMessage "Extracting 'db/${COMPRESSED_FILE}' to '${USER_FILES_PATH}'..."

# Delete the user files directory if it exists.
if [ -d "${USER_FILES_PATH}" ] ; then
  sudo rm -R -f "${USER_FILES_PATH}"
  mkdir "${USER_FILES_PATH}"
else
  mkdir "${USER_FILES_PATH}"
fi

# Extract and move to correct location.
cd "${PROJECT_ROOT}/db"
tar -xzf "${COMPRESSED_FILE}" -C "${USER_FILES_PATH}"

# Update file permsions.
sudo chmod -R 777 "${USER_FILES_PATH}"
