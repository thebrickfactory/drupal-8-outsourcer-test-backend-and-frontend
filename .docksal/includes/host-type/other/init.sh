#!/bin/bash

## Other init.sh script.

## Since we aren't connected directly to a hosting provider, we kind of have to
## guess what we are going to do for the build. Pantheon doesn't have any
## integrated build tools either, so let's just hit that code.

source "${PROJECT_ROOT}/.docksal/includes/host-type/pantheon/init.sh"
