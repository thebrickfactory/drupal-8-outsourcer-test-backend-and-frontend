# Docksal Hosting Integration Overrides

The code is setup to make customizations per project simpler. Below outlines the things you can change per project.

## Main Project Configuration
The main settings per project are defined in the `.docksal/docksal-local.env` file. This controls the base options you can set for each project. If configuration options are added, you may have to manually update this file with those changes, since this file is not overwritten when the .docksal files get updated from composer.

This is where you would change the php/myql version, set the host + site type values, etc...

## PHP Settings
Often times you will want to tweak certain php.ini settings for a project to match what is used on live. To do this update the `.docksal/etc/php/php-fpm.conf` and `.docksal/etc/php/php.ini` files and update those files directly. These files will not get overrwritten when pulling down changes.

## Local CMS Setting Files
When the `fin init` custom command runs, it will copy the default setting files, if those files aren't already on the site...depending on the site type that is defined for this project. These default files are setup to work with the Docksal environment. You have the option of adding your own setting files that will be used on initial setup instead of the default files for this project.

To do this open the `.docksal/settings` folder. From there you will see a folder for each CMS that is supported. Copy the file(s) under the sub directory that matches this project site type and paste them into the `.docksal/overrides` folder.

From there you can update these override files and anyone who checks out the repo going forward will use that file instead of the default file. Once these setting files are copied to your project, you can apply changes directly there. This is useful if you want to change some setting values on this project.

## Custom Command Overrides
Sometimes you may want to run additional commands when the custom commands are run for this project. The code is setup to look for certain files when running these custom commands. If they exist for this project, it will end up running these files at the end of that custom command. These are meant to be bash/shell scripting files, but in theory, you can write them in other language.

Here are a list of the files the system is looking for:

- `.docksal/overrides/init.sh`
- `.docksal/overrides/local.sh`
- `.docksal/overrides/import-db.sh`
- `.docksal/overrides/import-user-files.sh`
- `.docksal/overrides/deploy.sh`

Take a look at the files under `.docksal/commands` and `.docksal/includes` in what we are doing out of the box. If you create these files in this project, they will not get overriden with updates to this repo.
