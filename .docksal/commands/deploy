#!/bin/bash

## Manually deploys code to an environment.

# Grab time logger code.
source "${PROJECT_ROOT}/.docksal/includes/time-logger.sh"

# Define the variables that are used.
ENVIRONMENT="${1}"

if [ "${ENVIRONMENT}" == "" ] ; then
  # If environment is not set, use the default environment.
  ENVIRONMENT="${INTEGRATION_DEFAULT_ENVIRONMENT}"
fi

if [ "${INTEGRATION_SITE_ID}" == "" ] ; then
  faileMessage "Need to specify Integration Site ID! Stopping."
fi

if [ "${ENVIRONMENT}" == "" ] ; then
  faileMessage "Need to specify Platform.sh environment (branch) to push to! Stopping."
fi

# Inform the user.
startMessage "deploy (${INTEGRATION_HOST_TYPE}.${ENVIRONMENT}): push code to hosting environment"

cd "${PROJECT_ROOT}"

## Step #1: Run the host type local.sh script.
progressMessage "Deploying code to hosting environment..."
fin exec "${PATH_DOCKSAL_CLI}/${PATH_HOST_TYPE_DEPLOY} ${ENVIRONMENT}"

## Step #2: Run the override type deploy.sh script, if it is set.
if [ -f "${PROJECT_ROOT}/${PATH_OVERRIDE_TYPE_DEPLOY}" ] ; then
  progressMessage "deploy override detected..."
  fin exec "${PATH_DOCKSAL_CLI}/${PATH_OVERRIDE_TYPE_DEPLOY} ${ENVIRONMENT}"
fi

successMessage "Deployment finished!"

# Output time.
displayTime "deploy"
